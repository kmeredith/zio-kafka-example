package net

import zio._
import zio.duration._
import zio.console._
import zio.kafka.consumer._
import zio.kafka.serde._

object Main extends App {

  def run(args: List[String]): URIO[ZEnv, ExitCode] = {
    val settings: ConsumerSettings =
      ConsumerSettings(List("localhost:9092"))
        .withGroupId("group")
        .withClientId("client")
        .withCloseTimeout(30.seconds)


    val subscription = Subscription.topics("orders")

    Consumer.consumeWith[Any, Any, String, String](settings, subscription, Serde.string, Serde.string) { case (key, value) =>
      ZIO.effectTotal(println(s"Received message ${key}: ${value}"))
    }.orDie.as(ExitCode.success)
  }

}