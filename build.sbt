scalaVersion := "3.0.2"

libraryDependencies ++= Seq(
  "dev.zio" %% "zio-streams" % "1.0.12",
  "dev.zio" %% "zio-kafka"   % "0.17.1"
)